<?php
if (isset($_POST['email'])) {
    $email = $_POST['email'];
    $pass = $_POST['pass'];
    $query = 'SELECT * FROM registros WHERE correo = "'.$email.'" AND contraseña = "'.$pass.'"';
    $resultados = $this->db->query($query);
    if ($resultados -> num_rows() > 0) {
        foreach ($resultados->result() as $llave) {
            foreach ($llave as $key => $value) {
                $_SESSION[$key] = $value;
            }
        }
        header('Location: '.base_url(''));
    }else{
    }
}
if (!isset($_SESSION['nombre'])) {
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Iris</title>
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Estos son nuestros estilos -->
    <link rel="stylesheet" href="./css/index.css">
    <!-- Nuestras fuentes -->
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
</head>

<body>

<nav>
    <?php echo $navbar; ?> 
</nav>


    <?php echo $form; ?>
    <?php echo $footer; ?>

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <!--Aqui van nuestros js  -->
    <script src="./js/index.js"></script>
    <script>M.AutoInit();</script>
    
</body>

</html>
<?php
}else{
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Iris</title>
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Estos son nuestros estilos -->
    <link rel="stylesheet" href="./css/index.css">
    <!-- Nuestras fuentes -->
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
</head>

<body>

<nav>
    <?php echo $navbar; ?> 
</nav>

    <?php echo $mapa; ?>


    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <!--Aqui van nuestros js  -->
    <script src="./js/index.js"></script>
    <script>M.AutoInit();</script>
    
</body>

</html>

<?php
}
?>