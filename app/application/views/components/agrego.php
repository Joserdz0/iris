<div class="row">
    <div class="col s12 m6 push-m3">
      <div class="card red accent-4">
        <div class="card-content white-text">
          <span class="card-title">Advertencia</span>
          <p>Al estar añadiendo persona a la aplicación esta bajo tu responsabilidad saber a quien agregas y el mal uso que esta persona pueda dar a la aplicación puede afectarte a ti.</p>
        </div>
        <div class="card-action">
        <form action="<?php echo base_url('AgregarUsuarios');?>" method="post">
        <div class="input-field col s8">
          <input style="color:white;" id="number" name="number" type="number">
          <label style="color:white;" for="number">Numero</label>
        </div>
        <button type="submit" class="btn waves-effect waves-light blue darken-4">Validar codigo</button>
        </form>
        </div>
      </div>
    </div>
  </div>